Shader "Primer Shader"{
    SubShader{ //unity elige de cualquier de los subshader y elige el mejor para la gpu

        Pass{ // dependiendo el shader puede ser que se necesite procesar varias veces
            // aqui se hace uso de la logica en algun lenguaje de los shaders

            CGPROGRAM // Aqui lo que se dice es que lo que sigue va a ser en lenguaje CG

            //Se especifica nombres de funciones para vertex y fragmentshaders
            // Fragmento no es lo mismo que un pixe;, primero es el fragmento (cuadriculas) y la union o conjunto de fragmentos se hace pixel
            //Frgament shaders es el que da color

            #pragma vertex vert
            #pragma fragment frag

            //Se declara la funcion del  vertx shader

            float4 vert(float4 vertexPos : POSITION) : SV_POSITION
            {

                //float4 es un vector tamanio 4 de float
                //cada valor tiene 2 definidores tipo semantic
                //tipo define como se guarda en memoria
                //semantic define que representa / significa

                //se recibe los vertices en cordenadas LOCALES

                //UnityObjectToClipPos aplica una serie de transformaciones que lo colocan en la posicion que le corresponde
                //inclusive considerando camara 

                //esto se hizo en clase, el cos solo hace que regrese y vaya la figura
                

                //Movimiento Pescado

                
                float4 resultado = UnityObjectToClipPos(vertexPos);
                return float4 (cos(resultado.x + _Time.y)-4,resultado.y ,resultado.z,resultado.w);
                

                //Movimiento ballena
                

                /*
                float4 resultado = UnityObjectToClipPos(vertexPos);
                float4 resultado2 = float4 (resultado.x + cos,resultado.y,resultado.z,resultado.w );

                return resultado2;
                */
                //en un float4 podemos acceder a cada elemento por medio de 
                //x,y,z,w - posicion
                //r,g,b,a - color


            }

            float4 frag(void) : color
            {

                return float4(0.0, 1.0, 1.0, 1.0);

            }


            ENDCG //Hasta aqui se va a ser udo de CG 
        }


    }


}